// var player;
var sitTime;
var lastUp;
var is_sit = true;
getSitTime();
	
function updateTimer() {
    var currTime = Date.now();
    var elapsedTime;
    if (is_sit) {
        elapsedTime = new Date(currTime - sitTime);

        if (((currTime - sitTime) % (1 * 60000)) < 999 && ((currTime - sitTime) / (1 * 60000)) > 1) {//plus de 2h assis = 3600000
            player.playVideo();
        }
    }
    else {
        elapsedTime = new Date(currTime - lastUp);
    }
	
	document.getElementById("timer-h").textContent = pad(elapsedTime.getHours() + (new Date().getTimezoneOffset()/60), 2);
	document.getElementById("timer-m").textContent = pad(elapsedTime.getMinutes(), 2);
	document.getElementById("timer-s").textContent = pad(elapsedTime.getSeconds(), 2);
	
	setTimeout(updateTimer, 1000);
}

function updateTimerText() {
    if (sitTime > lastUp) {
        document.getElementById("timer-text").textContent = "Vous êtes assis depuis";
        is_sit = true;
        document.getElementById("countdown").classList.add("sit");
        document.getElementById("countdown").classList.remove("up");
    }
    else {
        document.getElementById("timer-text").textContent = "Vous n'êtes pas assis depuis";
        is_sit = false;
        document.getElementById("countdown").classList.add("up");
        document.getElementById("countdown").classList.remove("sit");
    }
}

function pad(num, size) {
    num = num.toString();
    while (num.length < size) num = "0" + num;
    return num;
}

function getSitTime() {
    fetch("devicetimes/" + device_id)
        .then(function(response) {
            return response.json().then(function(json) {
                sitTime = new Date(json.currentSit);
                lastUp = new Date(json.lastUp);

                updateTimerText();
                updateTimer();
            })
        })
        .catch(function (error) {
            console.log("Error: " + error);
        });
}

function onYouTubeIframeAPIReady() {
    player = new YT.Player('video-placeholder', {
        width: 600,
        height: 400,
        videoId: 'hdcTmpvDO0I',
    });
}

// Create a client instance
client = new Paho.MQTT.Client("rilm.tk", 1884, "");
client.onMessageArrived = onMqttMessage;

// connect the client
client.connect({onSuccess:onConnect});

// called when the client connects
function onConnect() {
  client.subscribe("chair/" + device_id);
}

// called when a message arrives
function onMqttMessage(message) {
  getSitTime();
}