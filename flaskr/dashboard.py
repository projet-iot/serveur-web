from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

import json

bp = Blueprint("dashboard", __name__,url_prefix="/dashboard")

@bp.route("/dashboard", methods=("GET", "POST"))
@login_required
def dashboard():
        ## a completer
        username= g.user["username"]
        user_id=g.user["id"]
        db = get_db()
        device_id=db.execute("SELECT id FROM device WHERE author_id = ?", str(user_id)).fetchone()
        return render_template("dashboard/index.html", username=username, device_id=device_id['id'])



@bp.route("/devicetimes/<device_id>", methods=("GET", ))
def devicetimes(device_id):
    db = get_db()
    timequery = db.execute("SELECT currentsit, lastUp FROM device WHERE id = ?", device_id)
    data = timequery.fetchone()
    return {"currentSit": data['currentSit'], "lastUp": data["lastUp"]}

